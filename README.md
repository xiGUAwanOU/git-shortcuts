# Git Shortcuts

Use following command to install:

```console
$ curl -s -L https://gitlab.com/xiGUAwanOU/git-shortcuts/-/raw/master/install | bash -
```

Scripts will be installed to the current working directory.
