#!/usr/bin/env bash

REMOTE=origin
[ -n "$1" ] && REMOTE=$1

git push ${REMOTE} HEAD
